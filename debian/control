Source: python-zeep
Maintainer: Debian Tryton Maintainers <team+tryton-team@tracker.debian.org>
Uploaders: Mathias Behrle <mathiasb@m9s.biz>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python (>= 1.20130901-1~),
               libxmlsec1,
               libxmlsec1-openssl,
               python3,
               python3-attr,
               python3-cached-property,
               python3-coverage,
               python3-flake8,
               python3-flake8-blind-except,
               python3-freezegun,
               python3-isodate,
               python3-isort,
               python3-lxml,
               python3-platformdirs,
               python3-pretend,
               python3-pytest,
               python3-pytest-cov,
               python3-pytest-asyncio,
               python3-pytest-tornado,
               python3-requests,
               python3-requests-file,
               python3-requests-mock,
               python3-requests-toolbelt,
               python3-setuptools,
               python3-tz
Standards-Version: 4.6.2
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/tryton-team/python-zeep
Vcs-Git: https://salsa.debian.org/tryton-team/python-zeep.git
Homepage: https://github.com/mvantellingen/python-zeep

Package: python3-zeep
Architecture: all
Depends: python3-attr,
         python3-cached-property,
         python3-defusedxml,
         python3-freezegun,
         python3-isodate,
         python3-lxml,
         python3-platformdirs,
         python3-pkg-resources,
         python3-requests,
         python3-requests-file,
         python3-requests-toolbelt,
         python3-tz,
         ${misc:Depends},
         ${python3:Depends}
Description: Modern SOAP client library (Python 3)
 A fast and modern Python SOAP client
 .
 Highlights:
  * Modern codebase compatible with Python versions >= 3.7 and PyPy
  * Build on top of lxml and requests
  * Supports recursive WSDL and XSD documents.
  * Supports the xsd:choice and xsd:any elements.
  * Uses the defusedxml module for handling potential XML security issues
  * Support for WSSE (UsernameToken only for now)
  * Experimental support for HTTP bindings
  * Experimental support for WS-Addressing headers
  * Experimental support for asyncio via aiohttp (Python 3.5+)
 .
 Features still in development include:
  * WSSE x.509 support (BinarySecurityToken)
  * WS Policy support
 .
 Please see for more information the documentation at
 http://docs.python-zeep.org/
 .
 This package is targeting Python version 3.
